

#The Makefile focusses on building an executable file for two target platform namely: HOST machine or MSP432. The required platform must be provided in the terminal commands by using PLATFORM overide. Different target rules produce different outputs. The description for the rules of the target files is provided in 'Build Targets:' below. Map files is produced for MSP432 platform only. Linker file is specified for 
#MSP432 only. Dependency file is created for both HOST and MSP432 target for all object files.

# Build Targets:
#	compile-all - Builds all source files but does not link. Requires source files as prerequistes
#	build - Builds all source files and links to an executable. Requires source files as  prerequisites
#	clean - Cleans all the generated files during the build process
# 	SIZE_REPORT - Used to produce a report on the size occupied in the memory segment for a build process of MSP432 platform. Here #	requires target executable and object files.
